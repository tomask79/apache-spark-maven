## Demo of Apache Spark application and how to prepare and work with Spark cluster ##

**Apache Spark is a distributed computation platform** which is very popular in these days, especially because of the **far better performance compare to Hadoop mapreduce, Spark is almost 100x faster** then disk based hadoop mapreduce. Let's test it and create maven Apache Spark application from scratch...

Apache Spark can run in three modes:

* Apache Spark application accessing filesystem (local file system, [HDFS](https://hadoop.apache.org/docs/r1.2.1/hdfs_design.html)).
* Apache Spark application accessing distributed systems like [HBase](https://hbase.apache.org/).
* Apache Spark application upon [Yarn](https://hadoop.apache.org/docs/current/hadoop-yarn/hadoop-yarn-site/YARN.html).

**We will test here first variant** with classic word count example for file **saved in the Hadoop HDFS filesystem**

### Prerequisities ###

* Installed Hadoop, I've got **Hadoop 2.6.0** at my Mac. For installation, follow instructions here for example...[Install Hadoop on Mac](http://amodernstory.com/2014/09/23/installing-hadoop-on-mac-osx-yosemite/)

* Uploaded text file into HDFS. For commands how to work with HDFS see [HDFS command guide](https://hadoop.apache.org/docs/r2.7.1/hadoop-project-dist/hadoop-hdfs/HDFSCommands.html)

* Installed Apache Spark. Spark can be downloaded in prebuilt version or you can build it manually. At Mac OS X, most easier way to install Apache Spark is with **homebrew**. I've got **Apache Spark 1.3.0** which works with **Hadoop 2.6.0** perfectly. See [Spark standalone mode](http://spark.apache.org/docs/latest/spark-standalone.html#installing-spark-standalone-to-a-cluster)

* **git clone <this repo>** and run **mvn clean install**

### Preparing and starting Apache Spark and Hadoop clusters ###

Before any of testing, we need the following things:

* We need to have Apache Spark master running...From folder /usr/local/Cellar/apache-spark/1.3.0/libexec/sbin run following command:

```
./start-master.sh 
```

This will start master node of the Apache Spark cluster. Spark offers excellent web console to see master node properties...From your browser, hit following URL:

```
http://localhost:8080/
```

To be able to run anything in Apache Spark cluster, we need to add at least one worker, to do this, from bin folder of apache spark run following command:

```
spark-class org.apache.spark.deploy.worker.Worker spark://tomask79.local:7077
```

**spark://tomask79.local:7077 is a master node at my Mac, change it to your master according the master node URL listed in localhost:8080.
**

Visit localhost:8080 again and you should see your worker listed in the workers table...Perfect, Apache Spark cluster is ready!

**We're going to be loading file at HDFS file system**, so we need to start Hadoop cluster, from folder /usr/local/Cellar/hadoop/2.6.0/libexec/sbin
start the hadoop cluster:

```
./start-all.sh
```

### Submitting application to Apache Cluster and examining the code ###

Every Apache Spark application needs to be properly configured before submitting it to Apache cluster, let's examine the configuration code:

```
    SparkConf sparkConf = new SparkConf();
    sparkConf.set("spark.cores.max", "1");
    sparkConf.set("spark.executor.memory", "2048M");
    sparkConf.set("spark.driver.memory", "1024M");
    sparkConf.setAppName("JavaWordCount");
    sparkConf.setMaster("spark://tomask79.local:7077");
```
To see the full list of Apache Spark application properties, see the following link: [Spark configuration](http://spark.apache.org/docs/latest/configuration.html)

Let's just say that **setting the master node URL with setMaster method is mandatory**. Other options are optional. But it's good to control how much resources application will eat at Apache Spark node. Otherwise you can run into evergreen rookie Apache Spark issue with error:

```
Initial job has not accepted any resources; check your cluster UI to ensure that workers are registered and have sufficient memory
```

Second extremely important configuration is the following code:

```
JavaSparkContext ctx = new JavaSparkContext(sparkConf);
ctx.addJar("/Users/tomask79/Documents/workspace/apache-  spark/spark/target/spark-0.0.1-SNAPSHOT.jar");
```
With this code you're telling **the Apache Spark driver where is the jar he should bring to worker nodes** to run the code there. Otherwise you end up with:

```
15/11/22 12:03:55 WARN TaskSetManager: Lost task 0.0 in stage 0.0 (TID 0, 192.168.1.112): java.lang.ClassNotFoundException: com.myspark.test.spark.JavaWordCount$1
	at java.net.URLClassLoader$1.run(URLClassLoader.java:366)
```

Rest of the code are simple [RDD](https://spark.apache.org/docs/0.8.1/api/core/org/apache/spark/rdd/RDD.html) commands performing mapreduce word count logic, no big deal, to understand it, visit following page: [Hadoop wordcount](https://wiki.apache.org/hadoop/WordCount)

Now you should be able to submit JavaWordCount application to Apache Spark cluster, if everything went well, after launching JavaWordCount class from IDE, you should see following output:

```
Using Spark's default log4j profile: org/apache/spark/log4j-defaults.properties
15/11/22 12:06:06 INFO SparkContext: Running Spark version 1.3.0
15/11/22 12:06:07 WARN NativeCodeLoader: Unable to load native-hadoop library for your platform... using builtin-java classes where applicable
15/11/22 12:06:07 INFO SecurityManager: Changing view acls to: tomask79
15/11/22 12:06:07 INFO SecurityManager: Changing modify acls to: tomask79
15/11/22 12:06:07 INFO SecurityManager: SecurityManager: authentication disabled; ui acls disabled; users with view permissions: Set(tomask79); users with modify permissions: Set(tomask79)
15/11/22 12:06:07 INFO Slf4jLogger: Slf4jLogger started
15/11/22 12:06:07 INFO Remoting: Starting remoting
15/11/22 12:06:08 INFO Remoting: Remoting started; listening on addresses :[akka.tcp://sparkDriver@192.168.1.112:52665]
15/11/22 12:06:08 INFO Utils: Successfully started service 'sparkDriver' on port 52665.
15/11/22 12:06:08 INFO SparkEnv: Registering MapOutputTracker
15/11/22 12:06:08 INFO SparkEnv: Registering BlockManagerMaster
15/11/22 12:06:08 INFO DiskBlockManager: Created local directory at /var/folders/bp/xx0g4jf92dj09627f16_jrym0000gp/T/spark-c3f3a1b4-6f2c-45a6-b7f2-074fa15c118e/blockmgr-00c30f36-201f-4383-a6eb-f3190beec5b4
15/11/22 12:06:08 INFO MemoryStore: MemoryStore started with capacity 1966.1 MB
15/11/22 12:06:08 INFO HttpFileServer: HTTP File server directory is /var/folders/bp/xx0g4jf92dj09627f16_jrym0000gp/T/spark-876f0fec-b2f3-4ad1-99fd-73af479108e2/httpd-a52ea639-a589-4fec-8dea-314cb00b6e66
15/11/22 12:06:08 INFO HttpServer: Starting HTTP Server
15/11/22 12:06:08 INFO Server: jetty-8.y.z-SNAPSHOT
15/11/22 12:06:08 INFO AbstractConnector: Started SocketConnector@0.0.0.0:52666
15/11/22 12:06:08 INFO Utils: Successfully started service 'HTTP file server' on port 52666.
15/11/22 12:06:08 INFO SparkEnv: Registering OutputCommitCoordinator
15/11/22 12:06:08 INFO Server: jetty-8.y.z-SNAPSHOT
15/11/22 12:06:08 INFO AbstractConnector: Started SelectChannelConnector@0.0.0.0:4040
15/11/22 12:06:08 INFO Utils: Successfully started service 'SparkUI' on port 4040.
15/11/22 12:06:08 INFO SparkUI: Started SparkUI at http://192.168.1.112:4040
15/11/22 12:06:08 INFO AppClient$ClientActor: Connecting to master akka.tcp://sparkMaster@tomask79.local:7077/user/Master...
15/11/22 12:06:09 INFO SparkDeploySchedulerBackend: Connected to Spark cluster with app ID app-20151122120609-0001
15/11/22 12:06:09 INFO AppClient$ClientActor: Executor added: app-20151122120609-0001/0 on worker-20151122114708-192.168.1.112-52585 (192.168.1.112:52585) with 1 cores
15/11/22 12:06:09 INFO SparkDeploySchedulerBackend: Granted executor ID app-20151122120609-0001/0 on hostPort 192.168.1.112:52585 with 1 cores, 2.0 GB RAM
15/11/22 12:06:09 INFO AppClient$ClientActor: Executor updated: app-20151122120609-0001/0 is now LOADING
15/11/22 12:06:09 INFO AppClient$ClientActor: Executor updated: app-20151122120609-0001/0 is now RUNNING
15/11/22 12:06:09 INFO NettyBlockTransferService: Server created on 52668
15/11/22 12:06:09 INFO BlockManagerMaster: Trying to register BlockManager
15/11/22 12:06:09 INFO BlockManagerMasterActor: Registering block manager 192.168.1.112:52668 with 1966.1 MB RAM, BlockManagerId(<driver>, 192.168.1.112, 52668)
15/11/22 12:06:09 INFO BlockManagerMaster: Registered BlockManager
15/11/22 12:06:09 INFO SparkDeploySchedulerBackend: SchedulerBackend is ready for scheduling beginning after reached minRegisteredResourcesRatio: 0.0
15/11/22 12:06:09 INFO SparkContext: Added JAR /Users/tomask79/Documents/workspace/apache-spark/spark/target/spark-0.0.1-SNAPSHOT.jar at http://192.168.1.112:52666/jars/spark-0.0.1-SNAPSHOT.jar with timestamp 1448190369686
15/11/22 12:06:09 INFO MemoryStore: ensureFreeSpace(138675) called with curMem=0, maxMem=2061647216
15/11/22 12:06:09 INFO MemoryStore: Block broadcast_0 stored as values in memory (estimated size 135.4 KB, free 1966.0 MB)
15/11/22 12:06:09 INFO MemoryStore: ensureFreeSpace(18512) called with curMem=138675, maxMem=2061647216
15/11/22 12:06:09 INFO MemoryStore: Block broadcast_0_piece0 stored as bytes in memory (estimated size 18.1 KB, free 1966.0 MB)
15/11/22 12:06:09 INFO BlockManagerInfo: Added broadcast_0_piece0 in memory on 192.168.1.112:52668 (size: 18.1 KB, free: 1966.1 MB)
15/11/22 12:06:09 INFO BlockManagerMaster: Updated info of block broadcast_0_piece0
15/11/22 12:06:09 INFO SparkContext: Created broadcast 0 from textFile at JavaWordCount.java:46
15/11/22 12:06:10 INFO FileInputFormat: Total input paths to process : 1
15/11/22 12:06:10 INFO SparkContext: Starting job: collect at JavaWordCount.java:79
15/11/22 12:06:11 INFO DAGScheduler: Registering RDD 3 (mapToPair at JavaWordCount.java:59)
15/11/22 12:06:11 INFO DAGScheduler: Got job 0 (collect at JavaWordCount.java:79) with 1 output partitions (allowLocal=false)
15/11/22 12:06:11 INFO DAGScheduler: Final stage: Stage 1(collect at JavaWordCount.java:79)
15/11/22 12:06:11 INFO DAGScheduler: Parents of final stage: List(Stage 0)
15/11/22 12:06:11 INFO DAGScheduler: Missing parents: List(Stage 0)
15/11/22 12:06:11 INFO DAGScheduler: Submitting Stage 0 (MapPartitionsRDD[3] at mapToPair at JavaWordCount.java:59), which has no missing parents
15/11/22 12:06:11 INFO MemoryStore: ensureFreeSpace(4400) called with curMem=157187, maxMem=2061647216
15/11/22 12:06:11 INFO MemoryStore: Block broadcast_1 stored as values in memory (estimated size 4.3 KB, free 1966.0 MB)
15/11/22 12:06:11 INFO MemoryStore: ensureFreeSpace(3099) called with curMem=161587, maxMem=2061647216
15/11/22 12:06:11 INFO MemoryStore: Block broadcast_1_piece0 stored as bytes in memory (estimated size 3.0 KB, free 1966.0 MB)
15/11/22 12:06:11 INFO BlockManagerInfo: Added broadcast_1_piece0 in memory on 192.168.1.112:52668 (size: 3.0 KB, free: 1966.1 MB)
15/11/22 12:06:11 INFO BlockManagerMaster: Updated info of block broadcast_1_piece0
15/11/22 12:06:11 INFO SparkContext: Created broadcast 1 from broadcast at DAGScheduler.scala:839
15/11/22 12:06:11 INFO DAGScheduler: Submitting 1 missing tasks from Stage 0 (MapPartitionsRDD[3] at mapToPair at JavaWordCount.java:59)
15/11/22 12:06:11 INFO TaskSchedulerImpl: Adding task set 0.0 with 1 tasks
15/11/22 12:06:13 INFO SparkDeploySchedulerBackend: Registered executor: Actor[akka.tcp://sparkExecutor@192.168.1.112:52672/user/Executor#-1456408878] with ID 0
15/11/22 12:06:13 INFO TaskSetManager: Starting task 0.0 in stage 0.0 (TID 0, 192.168.1.112, NODE_LOCAL, 1359 bytes)
15/11/22 12:06:13 INFO BlockManagerMasterActor: Registering block manager 192.168.1.112:52675 with 1060.3 MB RAM, BlockManagerId(0, 192.168.1.112, 52675)
15/11/22 12:06:13 INFO BlockManagerInfo: Added broadcast_1_piece0 in memory on 192.168.1.112:52675 (size: 3.0 KB, free: 1060.3 MB)
15/11/22 12:06:13 INFO BlockManagerInfo: Added broadcast_0_piece0 in memory on 192.168.1.112:52675 (size: 18.1 KB, free: 1060.3 MB)
15/11/22 12:06:14 INFO TaskSetManager: Finished task 0.0 in stage 0.0 (TID 0) in 1805 ms on 192.168.1.112 (1/1)
15/11/22 12:06:14 INFO TaskSchedulerImpl: Removed TaskSet 0.0, whose tasks have all completed, from pool 
15/11/22 12:06:14 INFO DAGScheduler: Stage 0 (mapToPair at JavaWordCount.java:59) finished in 3,757 s
15/11/22 12:06:14 INFO DAGScheduler: looking for newly runnable stages
15/11/22 12:06:14 INFO DAGScheduler: running: Set()
15/11/22 12:06:14 INFO DAGScheduler: waiting: Set(Stage 1)
15/11/22 12:06:14 INFO DAGScheduler: failed: Set()
15/11/22 12:06:14 INFO DAGScheduler: Missing parents for Stage 1: List()
15/11/22 12:06:14 INFO DAGScheduler: Submitting Stage 1 (ShuffledRDD[4] at reduceByKey at JavaWordCount.java:69), which is now runnable
15/11/22 12:06:14 INFO MemoryStore: ensureFreeSpace(2240) called with curMem=164686, maxMem=2061647216
15/11/22 12:06:14 INFO MemoryStore: Block broadcast_2 stored as values in memory (estimated size 2.2 KB, free 1966.0 MB)
15/11/22 12:06:14 INFO MemoryStore: ensureFreeSpace(1658) called with curMem=166926, maxMem=2061647216
15/11/22 12:06:14 INFO MemoryStore: Block broadcast_2_piece0 stored as bytes in memory (estimated size 1658.0 B, free 1966.0 MB)
15/11/22 12:06:14 INFO BlockManagerInfo: Added broadcast_2_piece0 in memory on 192.168.1.112:52668 (size: 1658.0 B, free: 1966.1 MB)
15/11/22 12:06:14 INFO BlockManagerMaster: Updated info of block broadcast_2_piece0
15/11/22 12:06:14 INFO SparkContext: Created broadcast 2 from broadcast at DAGScheduler.scala:839
15/11/22 12:06:14 INFO DAGScheduler: Submitting 1 missing tasks from Stage 1 (ShuffledRDD[4] at reduceByKey at JavaWordCount.java:69)
15/11/22 12:06:14 INFO TaskSchedulerImpl: Adding task set 1.0 with 1 tasks
15/11/22 12:06:14 INFO TaskSetManager: Starting task 0.0 in stage 1.0 (TID 1, 192.168.1.112, PROCESS_LOCAL, 1122 bytes)
15/11/22 12:06:14 INFO BlockManagerInfo: Added broadcast_2_piece0 in memory on 192.168.1.112:52675 (size: 1658.0 B, free: 1060.3 MB)
15/11/22 12:06:14 INFO MapOutputTrackerMasterActor: Asked to send map output locations for shuffle 0 to sparkExecutor@192.168.1.112:52672
15/11/22 12:06:14 INFO MapOutputTrackerMaster: Size of output statuses for shuffle 0 is 142 bytes
15/11/22 12:06:14 INFO DAGScheduler: Stage 1 (collect at JavaWordCount.java:79) finished in 0,098 s
15/11/22 12:06:14 INFO TaskSetManager: Finished task 0.0 in stage 1.0 (TID 1) in 96 ms on 192.168.1.112 (1/1)
15/11/22 12:06:14 INFO TaskSchedulerImpl: Removed TaskSet 1.0, whose tasks have all completed, from pool 
15/11/22 12:06:14 INFO DAGScheduler: Job 0 finished: collect at JavaWordCount.java:79, took 3,993679 s
rule: 1
spark: 1
totally: 1
Apache: 1
world!: 1
the: 1
15/11/22 12:06:14 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/metrics/json,null}
15/11/22 12:06:14 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/stages/stage/kill,null}
15/11/22 12:06:14 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/,null}
15/11/22 12:06:14 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/static,null}
15/11/22 12:06:14 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/executors/threadDump/json,null}
15/11/22 12:06:14 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/executors/threadDump,null}
15/11/22 12:06:14 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/executors/json,null}
15/11/22 12:06:14 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/executors,null}
15/11/22 12:06:14 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/environment/json,null}
15/11/22 12:06:14 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/environment,null}
15/11/22 12:06:15 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/storage/rdd/json,null}
15/11/22 12:06:15 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/storage/rdd,null}
15/11/22 12:06:15 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/storage/json,null}
15/11/22 12:06:15 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/storage,null}
15/11/22 12:06:15 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/stages/pool/json,null}
15/11/22 12:06:15 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/stages/pool,null}
15/11/22 12:06:15 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/stages/stage/json,null}
15/11/22 12:06:15 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/stages/stage,null}
15/11/22 12:06:15 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/stages/json,null}
15/11/22 12:06:15 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/stages,null}
15/11/22 12:06:15 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/jobs/job/json,null}
15/11/22 12:06:15 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/jobs/job,null}
15/11/22 12:06:15 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/jobs/json,null}
15/11/22 12:06:15 INFO ContextHandler: stopped o.s.j.s.ServletContextHandler{/jobs,null}
15/11/22 12:06:15 INFO SparkUI: Stopped Spark web UI at http://192.168.1.112:4040
15/11/22 12:06:15 INFO DAGScheduler: Stopping DAGScheduler
15/11/22 12:06:15 INFO SparkDeploySchedulerBackend: Shutting down all executors
15/11/22 12:06:15 INFO SparkDeploySchedulerBackend: Asking each executor to shut down
15/11/22 12:06:15 INFO OutputCommitCoordinator$OutputCommitCoordinatorActor: OutputCommitCoordinator stopped!
15/11/22 12:06:15 INFO MapOutputTrackerMasterActor: MapOutputTrackerActor stopped!
15/11/22 12:06:15 INFO MemoryStore: MemoryStore cleared
15/11/22 12:06:15 INFO BlockManager: BlockManager stopped
15/11/22 12:06:15 INFO BlockManagerMaster: BlockManagerMaster stopped
15/11/22 12:06:15 INFO SparkContext: Successfully stopped SparkContext
15/11/22 12:06:15 INFO RemoteActorRefProvider$RemotingTerminator: Shutting down remote daemon.
15/11/22 12:06:15 INFO RemoteActorRefProvider$RemotingTerminator: Remote daemon shut down; proceeding with flushing remote transports.
15/11/22 12:06:15 INFO RemoteActorRefProvider$RemotingTerminator: Remoting shut down.
```

### Recommendations about maven depencies ###

First off all, be sure you use the same version of spark_core dependency as it's your installed Apache Spark. For example, I've got Apache Spark 1.3.0 installed, so my example uses in pom.xml

```
	<dependency>
		<groupId>org.apache.spark</groupId>
		<artifactId>spark-core_2.10</artifactId>
		<version>1.3.0</version>
	</dependency>
```

I had servlet API compatibility issues with hadoop 2.6.0 client, so I use 2.2.0 hadoop client, which works fine with Apache Spark 1.3.0 core:

```
 	<dependency>
		<groupId>org.apache.hadoop</groupId>
		<artifactId>hadoop-client</artifactId>
		<version>2.2.0</version>
	</dependency>
```

I hope you like my Apache Spark demo, if you have any comments let me know at my twitter account, @tomask79.