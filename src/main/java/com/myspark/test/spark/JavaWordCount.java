/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.myspark.test.spark;

import scala.Tuple2;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public final class JavaWordCount {
  private static final Pattern SPACE = Pattern.compile(" ");

  public static void main(String[] args) throws Exception {
    SparkConf sparkConf = new SparkConf();
    sparkConf.set("spark.cores.max", "1");
    sparkConf.set("spark.executor.memory", "2048M");
    sparkConf.set("spark.driver.memory", "1024M");
    sparkConf.setAppName("JavaWordCount");
    sparkConf.setMaster("spark://tomask79.local:7077");
    JavaSparkContext ctx = new JavaSparkContext(sparkConf);
    ctx.addJar("/Users/tomask79/Documents/workspace/apache-spark/spark/target/spark-0.0.1-SNAPSHOT.jar");
    JavaRDD<String> lines = ctx.textFile("hdfs://localhost:9000/user/wordcount.txt", 1);
       
    JavaRDD<String> words = lines.flatMap(new FlatMapFunction<String, String>() {
		private static final long serialVersionUID = 1L;

		public Iterable<String> call(String t) throws Exception {
			// TODO Auto-generated method stub
	          return Arrays.asList(SPACE.split(t));
		}

	  });
    

      JavaPairRDD<String, Integer> ones = words.mapToPair(new PairFunction<String, String, Integer>() {
  			private static final long serialVersionUID = 1L;

  			public Tuple2<String, Integer> call(String t) throws Exception {
  				// TODO Auto-generated method stub
  				return new Tuple2<String, Integer>(t, 1);
  			}
      });

      
      JavaPairRDD<String, Integer> counts = ones.reduceByKey(new Function2<Integer, Integer, Integer>() {
		private static final long serialVersionUID = 1L;

		public Integer call(Integer v1, Integer v2) throws Exception {
			// TODO Auto-generated method stub
	          return v1 + v2;
		}
      });


      List<Tuple2<String, Integer>> output = counts.collect();
      for (Tuple2<?,?> tuple : output) {
        System.out.println(tuple._1() + ": " + tuple._2());
      }
      ctx.stop();
  }
}
